import React from 'react';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";

import CollectionItem from '../collection-item/collection-item.component';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    collection_preview: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '30px',
    },
    title: {
        marginBottom: '1em',
        marginTop: '1.4em',
        color: '#3f51b5'
    },

});

const CollectionPreviewComponent = ({title, items}) => {
    const classes = useStyles();
    return (
        <div className={classes.collection_preview}>
            <Typography className={classes.title} variant="h3" component="h2">
                {title.toUpperCase()}
            </Typography>
            <Grid container direction="row"
                  justify="flex-start"
                  alignItems="stretch"
                  className={classes.preview}>
                {items
                    .filter((item, idx) => idx < 4)
                    .map(item => (
                        <CollectionItem key={item.id} item={item}/>
                    ))}

            </Grid>
        </div>
    )
};

export default CollectionPreviewComponent;