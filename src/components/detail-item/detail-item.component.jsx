import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import EventSeatIcon from '@material-ui/icons/EventSeat';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocalGasStationIcon from '@material-ui/icons/LocalGasStation';
import PaymentIcon from '@material-ui/icons/Payment';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import {redirect} from "../../abstracts/variable.utils";
import {useHistory} from "react-router-dom";
import {createStructuredSelector} from "reselect";
import {selectCurrentUser} from "../../redux/user/user.selectors";
import {connect} from "react-redux";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles({
    list: {
        '& .MuiListSubheader-root': {
            fontSize: '2.875rem'
        },
        paddingTop: '3em'
    },
    button: {
        marginTop: '-54px',
        position: 'relative',
        padding: '14px 162px',
        fontSize: '1.0375rem',
        marginBottom: '4em'
    },
    button_wrapper:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const DetailItem = ({cartItem, currentUser}) => {
    const classes = useStyles();
    const {name, imageUrl, price, year, model, fuel, count, seats} = cartItem;
    const [anchorEl, setAnchorEl] = React.useState(null);

    let history = useHistory();
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const idPop = open ? 'simple-popover' : undefined;
    return (
        <Grid container
              direction="row"
              justify="center"
              alignItems="center"
        >
            <Grid item>
                <Grid direction="column"
                      justify="flex-start"
                      alignItems="center">
                    <Grid item>
                        <img src={imageUrl} alt='item'/>
                    </Grid>
                    <Grid item className={classes.button_wrapper}>
                        {
                            currentUser ?
                                <Button size="large" className={classes.button} onClick={() => redirect(history, '/checkout')}
                                        variant="outlined" color="primary">
                                    Rent
                                </Button> :
                                <Button aria-describedby={idPop} size="large" className={classes.button} onClick={handleClick}  inverted='true'
                                        variant="outlined" color="primary">
                                    Rent
                                </Button>
                        }

                        <Popover
                            id={idPop}
                            open={open}
                            anchorEl={anchorEl}
                            onClose={handleClose}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                        > <Typography className={classes.typography}>Please LogIn or Create Account</Typography>
                        </Popover>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item>
                <List subheader={<ListSubheader>{name} | {model}</ListSubheader>} className={classes.list}>
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <EventSeatIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Seat" secondary={seats}/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <CalendarTodayIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Year" secondary={year}/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <LocalGasStationIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Fuel" secondary={fuel}/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <PaymentIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="price per day" secondary={`${price} $`}/>
                    </ListItem>
                    <ListItem>
                        <ListItemAvatar>
                            <Avatar>
                                <EventAvailableIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="available in moment" secondary={count}/>
                    </ListItem>
                </List>
            </Grid>
        </Grid>
    )
};
const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
});

export default connect (mapStateToProps)(DetailItem);
