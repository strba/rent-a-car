import React from 'react';
import MenuItem from "../menu-item/menu-item.component";
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import {Link} from 'react-router-dom';


const useStyles = team =>({
    directory_menu: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between'
},
    button:{
        position: 'fixed',
        right: '2%',
        bottom: '5%',
        '& .MuiSvgIcon-root':{
            width: '2em',
            height: '2em'
        }
    }


});


class Directory extends React.Component {
    constructor(){
        super();

        this.state = {
            section: [
                {
                    title: 'economy',
                    imageUrl: 'https://www.rvsleasing.co.uk/gfx/vehicles/front_view/5-series-saloon-bms5-21.jpg',
                    id: 1,
                    linkUrl: 'rent/economy'
                },
                {
                    title: 'estate',
                    imageUrl: 'https://www.rvsleasing.co.uk/gfx/vehicles/front_view/a6-allroad-auar-21.jpg',
                    id: 2,
                    linkUrl: 'rent/estate'
                },
                {
                    title: 'luxury',
                    imageUrl: 'https://www.rvsleasing.co.uk/gfx/vehicles/front_view/continental-gt-coupe-begt-20.jpg',
                    id: 3,
                    linkUrl: 'rent/luxury'
                },
                {
                    title: 'SUV',
                    imageUrl: 'https://www.rvsleasing.co.uk/gfx/vehicles/front_view/g-class-megc-21a.jpg',
                    size: 'large',
                    id: 4,
                    linkUrl: 'rent/suv'
                },
                {
                    title: 'cargo',
                    imageUrl: 'https://www.rvsleasing.co.uk/gfx/vehicles/front_view/transit-custom-double-cab-in-foti-21a.jpg',
                    size: 'large',
                    id: 5,
                    linkUrl: 'rent/cargo'
                }]
        }
    }
    render(){
        const { classes } = this.props;


        return(
            <div className={classes.directory_menu}>
                {
                    this.state.section.map(({id, ...otherSectionProps}) => (
                        <MenuItem key={id} {...otherSectionProps}/>
                        ))
                }
                <IconButton className={classes.button} component={Link} to={'/admin'} color="primary" aria-label="admin">
                    <SupervisorAccountIcon />
                </IconButton>
            </div>
        )
    }

}
export default  withStyles(useStyles)(Directory);