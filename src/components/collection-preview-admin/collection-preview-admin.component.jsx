import React from 'react';

import CollectionItemAdmin from '../collection-item-admin/collection-item-admin.component';

import {makeStyles} from '@material-ui/core/styles';
import {Grid} from "@material-ui/core";
import AddButton from "../add-button/add-button.component";
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles({
    button_wrapper: {
        marginTop: '5em',
        display: '-webkit-inline-box',
    },
    title: {
        marginBottom: '1em',
        marginTop: '1.4em',
        color: '#3f51b5'
    },

});

const CollectionPreviewAdmin = ({ title, items, id }) => {
    const classes = useStyles();
    return(
        <div>
            <Typography className={classes.title} variant="h3" component="h2">
                {title.toUpperCase()}
            </Typography>
            <Grid container alignItems="stretch" className='preview'>
                {items
                    .map(item => (
                        <CollectionItemAdmin key={item.id} item={item} collectionId={id}/>
                    ))}
                <Grid item xs={12} sm={6} md={4} lg={3} className={classes.button_wrapper}>
                    <AddButton/>
                </Grid>
            </Grid>
        </div>
    )};

export default CollectionPreviewAdmin;