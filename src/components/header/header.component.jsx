import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {createStructuredSelector} from "reselect";
import {auth} from '../../firebase/firebase.utils';
import {selectCurrentUser} from "../../redux/user/user.selectors";
import {ReactComponent as Logo} from '../../assets/car-rental.svg';
import {makeStyles} from '@material-ui/core/styles';
import SettingsIcon from '@material-ui/icons/Settings';
import Avatar from "@material-ui/core/Avatar";


const useStyles = makeStyles({
    header: {
        height: '70px',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: '25px',
    },
    logo_container: {
        height: '100%',
        width: '70px',
        padding: '5px'
    },
    options: {
        width: '50%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        textTransform: 'uppercase',
    },
    option: {
        padding: '10px 15px',
        cursor: 'pointer',
        '& .MuiAvatar-colorDefault': {
            background: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(111,88,240,0.87718837535014) 23%, rgba(1,199,240,1) 100%)'
        }
    },
    display_name:{
        padding: '10px 15px',
        cursor: 'default',
    },
    settings_icon: {}
});


const Header = ({currentUser}) => {
    const classes = useStyles();

    return (
        <div className={classes.header}>
            <Link className={classes.logo_container} to='/' key='logo'>
                <Logo className='logo'/>
            </Link>
            <div className={classes.options}>
                <Link className={classes.option} to='/rent' key='rent'>
                    RENT
                </Link>
                {currentUser ? (
                    [
                        <div className={classes.option} onClick={() => auth.signOut()} key='signout'>
                            SIGN OUT
                        </div>,
                        <div className={classes.display_name}>
                            {currentUser.displayName}
                        </div>,
                        <Link className={classes.option} to='/settings' key='setting'>

                            <Avatar>
                                <SettingsIcon className={classes.settings_icon}/>
                            </Avatar>

                        </Link>
                    ]
                ) : (
                    <Link className={classes.option} to='/signin' key='signin'>
                        SIGN IN
                    </Link>
                )}
            </div>
        </div>
    )
};

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
});

export default connect(mapStateToProps)(Header);
