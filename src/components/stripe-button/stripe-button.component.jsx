import React from "react";
import StripeCheckout from "react-stripe-checkout"
import {firestore} from "../../firebase/firebase.utils";
import firebase from "firebase/app";
import {idCarcollections} from "../../abstracts/variable.utils";


const StripeCheckoutButton = ({price, cartItem, currentUser}) => {
    const priceForStripe = price * 100;
    const publishableKey = 'pk_test_51I48J6C5HNLDBEFzJG5iW7yUNylaXs3tKKDK4Zn3etThDAnPyNhVjxApvWSRkeabMQwk6ISWetJt6gkNhOdAG3co009ERs8K08'


const onToken = async token =>{
    let start_date = new Date()
    let new_date = start_date.setDate( start_date.getDate() + cartItem[0].quantity )
    let end_date = new Date (new_date);
    const rent = {
        start_date: new Date(),
        end_date: end_date,
        carID: cartItem[0].id,
        price: price/100,
        car: cartItem[0].name +' '+ cartItem[0].model,
        username: currentUser.displayName,
        email: currentUser.email
    }
    const item = cartItem[0];
    delete item.quantity

    const rentRef = firestore.doc(`rents/${token.id}`)
    const carRef = firestore.collection(`collections`).doc(idCarcollections[item.category])

    const snapShot = await rentRef.get();
    if (!snapShot.exists) {
        try {
            await rentRef.set({...rent});
            await carRef.update({
                items: firebase.firestore.FieldValue.arrayRemove({
                    ...item
                })
            });
            if(item.count > 0){
                item.count -= 1
            }
            await carRef.update({
                items: firebase.firestore.FieldValue.arrayUnion(

                    {...item}
                )
            });
        } catch (error) {
            console.log('error creating user', error.message);
    }}


}

    return(
    <StripeCheckout
        label='Pay Now'
        name='React Store'
        billingAddress
        shippingAddress
        description={`Your total is $${price}`}
        amount={priceForStripe}
        panelLabel='Pay Now'
        token={onToken}
        stripeKey={publishableKey}
    />
)
}


export default StripeCheckoutButton
