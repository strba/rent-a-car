import React from 'react';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    group: {
        position: 'relative',
        margin: '45px 0',

        '& .form-input': {
            background: 'none',
            backgroundColor: 'white',
            color: team => team.sub_color,
            fontSize: '18px',
            padding: '10px 10px 10px 5px',
            display: 'block',
            width: '100%',
            border: 'none',
            borderRadius: 0,
            borderBottom: `1px solid`,
            borderBottomColor: team => team.sub_color,
            margin: '25px 0',

            '& :focus': {
                outline: 'none',
            },

            '& :focus ~ .form-input-label': {
                top: team => team.top,
                fontSize: team => team.fontSize,
                color: team => team.main_color,
            }
        },

        '& input[type="password"]': {
            letterSpacing: '0.3em',
        },

        '& .form-input-label': {
            color: team => team.sub_color,
            fontSize: '16px',
            fontWeight: 'normal',
            position: 'absolute',
            pointerEvents: 'none',
            left: '5px',
            top: '10px',
            transition: '300ms ease all',

            '& .shrink': {
                top: team => team.top,
                fontSize: team => team.fontSize,
                color: team => team.main_color,
            }
        }
    }

});
const FormInput = ({handleChange, label, ...otherProps}) => {


    const team = {
        sub_color: 'grey',
        main_color: 'black',
        top: '-14px',
        fontSize: '12px',
    }

    const classes = useStyles(team);

    return (
        <div className={classes.group}>
            <input className='form-input' onChange={handleChange} {...otherProps} />
            {label ? (
                <label
                    className={`${
                        otherProps.value.length ? 'shrink' : ''
                    } form-input-label`}
                >
                    {label}
                </label>
            ) : null}
        </div>
    )
};

export default FormInput;