import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from "@material-ui/core/Avatar";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {Link} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    root: {
        display: '-webkit-box',
        '& .MuiAvatar-colorDefault': {
            background: 'linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(111,88,240,0.87718837535014) 23%, rgba(1,199,240,1) 100%)',
            margin: '0 auto'
        },
        width: '100%',
        '&:hover':{
            opacity: 0.9
        }
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    content: {
        textAlign: 'center'
    },
    cover: {
        width: 151,
    },
    controls: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center'
    },
    playIcon: {
        height: 38,
        width: 38,
    },
    settings_icon: {
    }
}));

const AddButton = ({children, isGoogleSignIn, ...otherProps}) =>{
    const classes = useStyles();

    return (
        <Card className={classes.root} component={Link}  to='/admin/add'>
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h5">
                        ADD NEW CAR
                    </Typography>
                </CardContent>
                <div className={classes.controls}>
                        <Avatar>
                            <AddCircleOutlineIcon className={classes.settings_icon}/>
                        </Avatar>
                </div>
            </div>
        </Card>
    )};

export default AddButton;