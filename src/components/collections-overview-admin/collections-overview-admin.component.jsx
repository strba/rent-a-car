import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';

import CollectionPreviewAdmin from "../collection-preview-admin/collection-preview-admin.component";

import {selectCollectionsForPreview} from '../../redux/rent/rent.selectors';

import {makeStyles} from '@material-ui/core/styles';



const useStyles = makeStyles({
    collections_overview: {
        display: 'flex',
        flexDirection: 'column',
    }

})
;


const CollectionsOverviewAdmin = ({collections}) => {
    const classes = useStyles();
    return (
        <div className={classes.collections_overview}>
            {collections.map(({id, ...otherCollectionProps}) => (
                <CollectionPreviewAdmin key={id} {...otherCollectionProps} id={id}/>
            ))}
        </div>
    )
};

const mapStateToProps = createStructuredSelector({
    collections: selectCollectionsForPreview
});

export default connect(mapStateToProps)(CollectionsOverviewAdmin);