import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {addItem} from '../../redux/cart/cart.actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Grid} from "@material-ui/core";
import {firestore} from "../../firebase/firebase.utils";
import {createStructuredSelector} from "reselect";
import {selectCollectionsForPreview} from "../../redux/rent/rent.selectors";
import firebase from "firebase/app";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: '5em',
        height: '100%',
    },
    cart: {
        display: '-webkit-box;',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: '100%',
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
}));

const CollectionItemAdmin = ({item, addItem, history, collectionId}) => {
    const classes = useStyles();

    const {name, imageUrl, model} = item;

    const removeCar = async (collectionId) => {
        let carRef = firestore.collection(`collections`).doc(`${collectionId}`)
        await carRef.update({
            items: firebase.firestore.FieldValue.arrayRemove({
                ...item
            })
        });
    }

    return (
        <Grid item xs={12} sm={6} md={4} lg={3} className={classes.root}>
            <Card className={classes.cart}>
                <div className={classes.details}>
                    <CardContent className={classes.content}>
                        <Typography component="h5" variant="h5">
                            {name}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            {model}
                        </Typography>
                    </CardContent>
                    <div className={classes.controls}>
                        <IconButton onClick={() => {addItem(item);history.push('/admin/edit/');}} aria-label="edit">
                            <EditIcon/>
                        </IconButton>
                        <IconButton onClick={() => removeCar(collectionId)} aria-label="delete"
                                    className={classes.margin}>
                            <DeleteIcon fontSize="large"/>
                        </IconButton>
                    </div>
                </div>
                <CardMedia
                    className={classes.cover}
                    image={imageUrl}
                    title={`${name}  ${model} image`}
                />
            </Card>
        </Grid>
    );
};

const mapDispatchToProps = dispatch => ({
    addItem: item => dispatch(addItem(item)),
});
const mapStateToProps = createStructuredSelector({
    collections: selectCollectionsForPreview
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CollectionItemAdmin));
