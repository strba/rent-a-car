import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Grid} from "@material-ui/core";
import CustomButton from '../custom-button/custom-button.component';
import {addItem, clearItemFromCart} from '../../redux/cart/cart.actions';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

import {makeStyles} from '@material-ui/core/styles';
import {createStructuredSelector} from "reselect";
import {selectCurrentUser} from "../../redux/user/user.selectors";


const useStyles = makeStyles({
    collection_item: {
        marginTop: '2em',
        display: 'flex',
        flexDirection: 'column',
        height: '350px',
        alignItems: 'center',
        position: 'relative',
        '& .image': {
            width: '100%',
            height: '95%',
            backgroundSize: 'cover',
            backgroundPosition: 'center',
            marginBottom: '5px',
        },
        '& button': {
            width: '80%',
            opacity: '0.7',
            position: 'absolute',
            top: '255px',
            display: 'none',
        },
        '&:hover': {
            '& button': {
                opacity: 0.85,
                display: 'flex'
            },
            '& .image': {
                opacity: 0.8,
            }
        }
    },
    collection_footer: {
        width: '100%',
        height: '5%',
        display: 'flex',
        fontSize: '18px',
    },
    name: {
        textAlign: 'center',
        width: '50%',
        marginBottom: '15px',
    },
    price: {
        textAlign: 'center',
        width: '50%',
    },
});

const CollectionItem = ({item, addItem, history, currentUser}) => {
    const classes = useStyles();
    const {name, price, imageUrl, id} = item;
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const idPop = open ? 'simple-popover' : undefined;
    return (
        <Grid item xs={6} sm={4} md={3} className={classes.collection_item}>
            <div
                className="image"
                style={{
                    backgroundImage: `url(${imageUrl})`
                }}
                onClick={() => {
                    addItem(item);
                    history.push('/detail/' + id);
                }}
            />
            <div className={classes.collection_footer}>
                <span className={classes.name}>{name}</span>
                <span className={classes.price}>day | {price} $</span>
            </div>
            {
                currentUser ?
                    <CustomButton onClick={() => {
                        addItem(item);
                        history.push('/checkout');
                    }} inverted='true'>
                        Rent
                    </CustomButton> :
                    <CustomButton aria-describedby={idPop} onClick={handleClick}
                                  inverted='true'>Rent</CustomButton>
            }

            <Popover
                id={idPop}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            > <Typography className={classes.typography}>Please LogIn or Create Account</Typography>
            </Popover>
        </Grid>
    );
};

const mapDispatchToProps = dispatch => ({
    addItem: item => dispatch(addItem(item)),
    clearItem: item => dispatch(clearItemFromCart(item)),
});

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CollectionItem));
