import React from 'react';
import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import {createStructuredSelector} from "reselect";
import {selectCurrentUser} from "../../redux/user/user.selectors";
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {auth, firestore} from "../../firebase/firebase.utils";
import {useHistory} from "react-router-dom";



const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

const CurrentUserSettings = ({currentUser}) => {

    const {displayName, email, phoneNumber, id} = currentUser ? currentUser : '';


    const [formData, updateFormData] = React.useState({});
    const handleChange = (e) => {
        updateFormData({
            ...formData,
            [e.target.name]: e.target.value.trim()
        });
    };
    const classes = useStyles();
    let history = useHistory();
    const redirect = () => {
        history.push("/");
    }

    const removeUser = async itemId => {
        const userRef = firestore.doc(`/users/${itemId}`);
        try {
            await userRef.delete();
            await auth.signOut()
            redirect()
        } catch (error) {
            console.error('error creating user', error.message);
        }

    }

    const updateUser = async (itemId, data) => {
        const userRef = firestore.doc(`/users/${itemId}`);

        await userRef.update({
            displayName: data.displayName,
            email: data.email,
            phoneNumber: data.phoneNumber
        });
    }

    return (
        <div>
            <Typography variant="h3" gutterBottom>
                Edit User
            </Typography>
            {currentUser ? (
                <form className={classes.root} autoComplete="off">
                    <div>
                        <TextField
                            required
                            name="displayName"
                            id="outlined-required"
                            label="Display Name"
                            defaultValue={currentUser ? displayName : ''}
                            variant="outlined"
                            key="edit-settings-name"
                            onChange={handleChange}
                        />
                        <TextField
                            required
                            name="email"
                            id="outlined-required"
                            label="Email address"
                            defaultValue={currentUser ? email : ''}
                            variant="outlined"
                            key="edit-settings-email"
                            onChange={handleChange}
                        />
                        <TextField
                            required
                            name="phoneNumber"
                            id="outlined-required"
                            label="Phone number"
                            defaultValue={currentUser ? phoneNumber : ''}
                            variant="outlined"
                            key="edit-settings-phone"
                            onChange={handleChange}
                        />
                    </div>
                    <Button onClick={() => updateUser(id, formData)} variant="outlined" color="primary"
                            startIcon={<EditIcon/>}>
                        Update
                    </Button>
                    <Button
                        onClick={() => removeUser(id)}
                        variant="outlined"
                        color="secondary"
                        startIcon={<DeleteIcon/>}>
                        Delete
                    </Button>
                </form>) : ''}
        </div>
    )
}

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser
});


export default connect(mapStateToProps)(CurrentUserSettings);