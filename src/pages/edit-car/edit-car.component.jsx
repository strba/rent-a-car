import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import {firestore} from "../../firebase/firebase.utils";
import firebase from "firebase";
import {useHistory} from "react-router-dom";
import {createStructuredSelector} from "reselect";
import {selectCartItems} from "../../redux/cart/cart.selectors";
import {connect} from "react-redux";
import {makeId, redirect, categories_option, fuel_types, idCarcollections} from "../../abstracts/variable.utils";
import {Grid} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
        '& .MuiFormHelperText-root':{
            color:'rgba(63, 81, 181,0.74)'
        },
        marginBottom: '1.5em'
    },
    title_headline:{
        marginBottom: '1em',
        marginTop: '1em',
        color: '#3f51b5'
    },
    button_wrapper:{
        marginTop: '1.5em'
    },
    button:{
        margin:8,
        width: '-webkit-fill-available'
    },
    img_input_wrapper:{
        "& .MuiTextField-root":{
            width: '-webkit-fill-available'
        }
    }
}));



const EditCarPage = ({cartItems}) => {

    const [vehicleData, updateVehicleData] = React.useState({});

    let history = useHistory();

    const {
        category,
        fuel,
        imageUrl,
        model,
        name,
        price,
        seats,
        year,
        count
    } = cartItems[0];

    const item = cartItems[0];
    delete item.quantity


    const handleChangeInput = (event) => {
        updateVehicleData({
            ...vehicleData,
            [event.target.name]: event.target.value.trim(),
            [event.target.name]: event.target.type === 'number' ? parseInt(event.target.value) : event.target.value,
            id: makeId(),
        });
    }

    const UpdateCar = async () => {
        const carRef = firestore.collection(`collections`).doc(idCarcollections[item.category])

        try {
            const mergeVehicleData = {
                ...item,
                ...vehicleData,
            };
            await carRef.update({
                items: firebase.firestore.FieldValue.arrayRemove({
                    ...item
                })
            });
            await carRef.update({
                items: firebase.firestore.FieldValue.arrayUnion(
                    {...mergeVehicleData}
                )
            });
            redirect(history, '/admin')
        } catch (e) {
            console.log(e)
        }
    }
    const classes = useStyles();
    return (
        <Grid container
              direction="column"
              justify="center"
              alignItems="center"
        >
            <Typography className={classes.title_headline} variant="h3" component="h1">
                Add New Vehicle
            </Typography>
            <form className={classes.root} autoComplete="off">
                <div>
                    <TextField
                        required
                        id="outlined-multiline-flexible"
                        name="name"
                        defaultValue={name}
                        label="Brand"
                        helperText="Update Brand of Vehicle"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                    <TextField
                        required
                        name="model"
                        defaultValue={model}
                        id="outlined-multiline-flexible"
                        label="Model"
                        helperText="Update Model of Vehicle"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div>
                    <TextField
                        id="outlined-helperText"
                        label="Year"
                        name="year"
                        defaultValue={year}
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                    <TextField
                        id="outlined-helperText"
                        label="Count"
                        name="count"
                        defaultValue={count}
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div>
                    <TextField
                        id="outlined-select-currency"
                        select
                        name="fuel"
                        defaultValue={fuel}
                        label="Select"
                        onChange={handleChangeInput}
                        helperText="Update fuel"
                        variant="outlined"
                    >
                        {fuel_types.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        id="outlined-select-currency"
                        select
                        name="category"
                        label="Select"
                        defaultValue={category}
                        onChange={handleChangeInput}
                        helperText="Update category"
                        variant="outlined"
                    >
                        {categories_option.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                <div>
                    <TextField
                        id="outlined-helperText"
                        label="Seats"
                        name="seats"
                        defaultValue={seats}
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                    <TextField
                        id="outlined-helperText"
                        label="Price"
                        defaultValue={price}
                        name="price"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div className={classes.img_input_wrapper}>
                    <TextField
                        id="outlined-start-adornment"
                        name="imageUrl"
                        defaultValue={imageUrl}
                        label="Image URL"
                        helperText="Update image url"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div className={classes.button_wrapper}>
                <Button size="large" className={classes.button} onClick={() => UpdateCar()} variant="outlined" color="primary"
                        startIcon={<EditIcon/>}>
                    Update
                </Button>
                </div>
            </form>
        </Grid>
    );
};

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems
});

export default connect(mapStateToProps)(EditCarPage)


