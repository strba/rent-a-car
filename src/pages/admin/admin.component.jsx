import React from 'react';
// import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import CollectionsOverviewAdmin from "../../components/collections-overview-admin/collections-overview-admin.component";
import {fetchCollectionsStartAsync} from "../../redux/rent/rent.actions";


// const useStyles = makeStyles({
//
// });


class AdminPage extends React.Component {
    componentDidMount() {
        const { fetchCollectionsStartAsync } = this.props;

        fetchCollectionsStartAsync();
    }

    render() {
    return(
        <CollectionsOverviewAdmin/>
    )}
}

const mapDispatchToProps = dispatch => ({
    fetchCollectionsStartAsync: () => dispatch(fetchCollectionsStartAsync())
});

export default connect(
    null,
    mapDispatchToProps
)(AdminPage);
