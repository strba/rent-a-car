import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import Button from "@material-ui/core/Button";
import {firestore} from "../../firebase/firebase.utils";
import firebase from "firebase";
import {useHistory} from "react-router-dom";
import {makeId,redirect, itemID, fuel_types, categories_option} from "../../abstracts/variable.utils";
import {Grid} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '25ch',
        },
        '& .MuiFormHelperText-root':{
            color:'rgba(63, 81, 181,0.74)'
        },
        marginBottom: '1.5em'
    },
    title_headline:{
        marginBottom: '1em',
        marginTop: '1em',
        color: '#3f51b5'
    },
    button_wrapper:{
        marginTop: '1.5em'
    },
    button:{
        margin:8,
        width: '-webkit-fill-available'
    },
    img_input_wrapper:{
        "& .MuiTextField-root":{
            width: '-webkit-fill-available'
        }
    }
}));


const AddCarPage = () => {

    const [newVehicleData, setVehicleData] = React.useState({});

    let history = useHistory();

    const handleChangeInput = (event) => {
        setVehicleData({
            ...newVehicleData,
            count: 1,
            // Trimming any whitespace
            [event.target.name]: event.target.value.trim(),
            [event.target.name]: event.target.type === 'number' ? parseInt(event.target.value) : event.target.value,
            id: makeId(),
        });
    }


    const CreateCar = async () => {
        const carRef = firestore.doc(`/collections/${itemID[newVehicleData.category].id}`);
        try {
            await carRef.update({
                items: firebase.firestore.FieldValue.arrayUnion(
                    {...newVehicleData}
                )
            });
            redirect(history, '/admin')
        }catch (e){
            console.log(e)
        }
    }
    const classes = useStyles();
    return (
        <Grid  container
               direction="column"
               justify="center"
               alignItems="center"
        >
            <Typography className={classes.title_headline} variant="h3" component="h1">
               Add New Vehicle
            </Typography>
            <form className={classes.root} autoComplete="off">
                <div>
                    <TextField
                        required
                        id="outlined-helperText"
                        name="name"
                        label="Brand"
                        helperText="Some important text"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                    <TextField
                        required
                        name="model"
                        id="outlined-helperText"
                        label="Model"
                        helperText="Some important text"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div>
                    <TextField
                        id="outlined-helperText"
                        label="Year"
                        name="year"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div>
                    <TextField
                        id="outlined-select-currency"
                        select
                        name="fuel"
                        label="Select"
                        onChange={handleChangeInput}
                        helperText="Please select fuel"
                        variant="outlined"
                    >
                        {fuel_types.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        id="outlined-select-currency"
                        select
                        name="category"
                        label="Select"
                        onChange={handleChangeInput}
                        helperText="Please select category"
                        variant="outlined"
                    >
                        {categories_option.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </div>
                <div>
                    <TextField
                        id="outlined-helperText"
                        label="Seats"
                        name="seats"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                    <TextField
                        id="outlined-helperText"
                        label="Price"
                        name="price"
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div className={classes.img_input_wrapper}>
                    <TextField
                        id="outlined-helperText"
                        name="imageUrl"
                        label="Image URL"
                        helperText="Please add image url"
                        onChange={handleChangeInput}
                        variant="outlined"
                    />
                </div>
                <div className={classes.button_wrapper}>
                    <Button size="large" className={classes.button} onClick={() => CreateCar()} variant="outlined" color="primary"
                            startIcon={<LibraryAddIcon/>}>
                        ADD
                    </Button>
                </div>
            </form>
        </Grid>
    );
};

export default AddCarPage
