import React from 'react';
import { connect } from 'react-redux';
import {createStructuredSelector} from "reselect";
import {selectCartItems} from "../../redux/cart/cart.selectors";
import DetailItem from "../../components/detail-item/detail-item.component";


const Detail = ({cartItems}) => (
    <div className='shop-page'>
        {cartItems.map(cartItem => (
            <DetailItem key={cartItem.id} cartItem={cartItem}/>
        ))}
    </div>
)

const mapStateToProps = createStructuredSelector({
    cartItems: selectCartItems,
});

export default connect(
    mapStateToProps
)(Detail);