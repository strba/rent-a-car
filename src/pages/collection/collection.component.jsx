import React from 'react';
import {connect} from 'react-redux';

import CollectionItem from '../../components/collection-item/collection-item.component';

import {selectCollection} from '../../redux/rent/rent.selectors';

import {makeStyles} from '@material-ui/core/styles';
import {Grid} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    collection_page: {
        display: 'flex',
        flexDirection: 'column',
    },
    title: {
        marginBottom: '1em',
        marginTop: '1em',
        color: '#3f51b5'
    },
});

const CollectionPage = ({collection}) => {
    const {title, items} = collection;
    const classes = useStyles();

    return (
        <div className={classes.collection_page}>
            <Typography className={classes.title} variant="h3" component="h2">
                {title.toUpperCase()}
            </Typography>
            <Grid container direction="row"
                  justify="flex-start"
                  alignItems="stretch">
                {items.map(item => (
                    <CollectionItem key={item.id} item={item}/>
                ))}
            </Grid>
        </div>
    );
};

const mapStateToProps = (state, ownProps) => ({
    collection: selectCollection(ownProps.match.params.collectionId)(state)
});

export default connect(mapStateToProps)(CollectionPage);