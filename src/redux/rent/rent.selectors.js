import { createSelector } from 'reselect';

const selectRent = state => state.rent;

export const selectCollections = createSelector(
    [selectRent],
    rent => rent.collections
);

export const selectCollectionsForPreview = createSelector(
    [selectCollections],
    collections =>
        collections ? Object.keys(collections).map(key => collections[key]) : []
);

export const selectCollection = collectionUrlParam =>
    createSelector(
        [selectCollections],
        collections => (collections ? collections[collectionUrlParam] : null)
    );

export const selectIsCollectionFetching = createSelector(
    [selectRent],
    rent => rent.isFetching
);

export const selectIsCollectionsLoaded = createSelector(
    [selectRent],
    rent => !!rent.collections
);
