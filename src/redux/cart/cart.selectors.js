import {createSelector} from 'reselect';

const selectCart = state => state.cart;

export const selectCartItems = createSelector(
    [selectCart],
    cart => cart.cartItems
);

// export const selectCartHidden = createSelector(
//     [selectCart],
//     cart => cart.hidden
// );

export const selectCartItemsCount = createSelector(
    [selectCartItems],
    cartItems =>
        cartItems.reduce(
            (accumalatedQuantity, cartItem) =>
                accumalatedQuantity + cartItem.quantity,
            0
        )
);
export const selectCartTotal = createSelector(
    [selectCartItems],
    cartItems =>

        cartItems.reduce(
            (accumalatedQuantity, cartItem) =>

                cartItem.quantity >= 10 ?
                    accumalatedQuantity + cartItem.quantity * (cartItem.price - (cartItem.price / 100) * 10) :
                    cartItem.quantity >= 5 ?
                        accumalatedQuantity + cartItem.quantity * (cartItem.price - (cartItem.price / 100) * 7) :
                        cartItem.quantity >= 3 ?
                            accumalatedQuantity + cartItem.quantity * (cartItem.price - (cartItem.price / 100) * 5) :
                            accumalatedQuantity + cartItem.quantity * cartItem.price
            , 0
        )
);