export const makeId = () => {
    let ID = "";
    let characters = "123456789123456789123456789123456789123456789";
    for (let i = 0; i < 12; i++) {
        ID += characters.charAt(Math.floor(Math.random() * 36));
    }
    return parseInt(ID);
}

export const redirect = (history, url) => {
    history.push(url);
}

export const fuel_types = [
    {
        value: 'Petrol',
        label: 'petrol',
    },
    {
        value: 'Diesel',
        label: 'diesel',
    },
    {
        value: 'Hybrid',
        label: 'hybrid',
    },
    {
        value: 'Electric',
        label: 'electric',
    },
];

export const categories_option = [
    {
        value: 'Economy',
        label: 'economy',
    },
    {
        value: 'Estate',
        label: 'estate',
    },
    {
        value: 'Luxury',
        label: 'luxury',
    },
    {
        value: 'SUV',
        label: 'SUV',
    },
    {
        value: 'Cargo',
        label: 'cargo'
    }
]
export const itemID =
    {
        Economy: {
            id: '1c5VMNa9RVZjv0aZYZj5',

        },
        Estate: {
            id: 'H2X7gzSweIKJnABbmGUX'

        },
        Luxury: {
            id: 'rO3rJXML6Cq2yjHgjcSK'
        },


        SUV: {
            id: 'Ae3xi2JDFJ2zyNOvGrqh'
        },
        Cargo: {
            id: 'tC9uTuNBzfXjKQnDrWhH'

        }
    }
export const idCarcollections = {

    Economy: '1c5VMNa9RVZjv0aZYZj5',
    SUV: 'Ae3xi2JDFJ2zyNOvGrqh',
    Estate: 'H2X7gzSweIKJnABbmGUX',
    Luxury: 'rO3rJXML6Cq2yjHgjcSK',
    Cargo: 'tC9uTuNBzfXjKQnDrWhH'

}