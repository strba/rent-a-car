import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import './App.css';

import HomePage from './pages/homepage/homepage.component';
import RentPage from './pages/rent/rent.component';
import SignInAndSignUpPage from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';
import CheckoutPage from './pages/checkout/checkout.component';
import Detail from "./pages/details/details.component";
import Header from './components/header/header.component';
import CurrentUserSettings from "./pages/user-settings/current-user-settings.component";
import AdminPage from "./pages/admin/admin.component";
import AddCarPage from "./pages/add-car/add-car.component";

import { auth, createUserProfileDocument } from './firebase/firebase.utils';

import { setCurrentUser } from './redux/user/user.actions';
import { selectCurrentUser } from './redux/user/user.selectors';

import {selectCollectionsForPreview} from "./redux/rent/rent.selectors";
import EditCarPage from "./pages/edit-car/edit-car.component";
class App extends React.Component {
    unsubscribeFromAuth = null;

    componentDidMount() {
        const { setCurrentUser } = this.props;

        this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
            if (userAuth) {
                const userRef = await createUserProfileDocument(userAuth);

                userRef.onSnapshot(snapShot => {
                    setCurrentUser({
                        id: snapShot.id,
                        ...snapShot.data()
                    });
                });
            }

            setCurrentUser(userAuth);
            // Remove comments on line if you need firebase util function for add collection to Cloud Firestore
            // addCollectionAndDocuments('collections',collectionsArray.map(({title,items})=> ({title,items})))
        });
    }

    componentWillUnmount() {
        this.unsubscribeFromAuth();
    }

    render() {
        return (
            <div>
                <Header />
                <Switch>
                    <Route exact path='/' component={HomePage} />
                    <Route path='/rent' component={RentPage} />
                    <Route exact path='/checkout'  component={CheckoutPage} />
                    <Route
                        exact
                        path='/signin'
                        render={() =>
                            this.props.currentUser ? (
                                <Redirect to='/' />
                            ) : (
                                <SignInAndSignUpPage />
                            )
                        }
                    />
                    <Route exact path='/detail/:id' component={Detail} />
                    <Route exact path='/settings' component={CurrentUserSettings} />
                    <Route exact path='/admin' component={AdminPage} />
                    <Route exact path='/admin/add' component={AddCarPage} />
                    <Route exact path='/admin/edit' component={EditCarPage} />
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
    collectionsArray: selectCollectionsForPreview
});

const mapDispatchToProps = dispatch => ({
    setCurrentUser: user => dispatch(setCurrentUser(user))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);